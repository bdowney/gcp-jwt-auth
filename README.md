# Google OpenID Connect
Authenticate a pipeline job with GCP OpenID Connect

# Steps
1. Create a `terraform.tfvars` file to set project values.
   gitlab_url            = "https://gitlab.example.com"
   gitlab_project_id     = "20"
   gitlab_namespace_path = "twitter"
2. Ensure you have GCP authentication environment variables for terraform.
   Example:
   ```
   GOOGLE_CREDENTIALS="{json key to create the openid provider using terraform}"
   GOOGLE_PROJECT="google-project-name"
   GOOGLE_REGION="us-west1"
   ```
3. Run `terraform apply`
4. Set the output of terraform as environment variables in the GitLab project. `GCP_SERVICE_ACCOUNT` and `GCP_WORKLOAD_IDENTITY_PROVIDER`
5. Run a pipeline to validate the authentication is working with `id_tokens:`