variable "gitlab_url" {
  type    = string
  default = "https://gitlab.com"
}

variable "gitlab_project_id" {
  type        = string
  description = "Project ID to restrict authentication from."
}

variable "gitlab_namespace_path" {
  type        = string
  description = "Namespace Path to Filter Auth Requests"
}
resource "random_id" "random" {
  byte_length = 4
}

resource "google_iam_workload_identity_pool" "gitlab-pool" {
  workload_identity_pool_id = "gitlab-pool-${random_id.random.hex}"
}

resource "google_iam_workload_identity_pool_provider" "gitlab-provider-jwt" {
  workload_identity_pool_id          = google_iam_workload_identity_pool.gitlab-pool.workload_identity_pool_id
  workload_identity_pool_provider_id = "gitlab-jwt-${random_id.random.hex}"
  attribute_condition                = "assertion.namespace_path.startsWith(\"${var.gitlab_namespace_path}\")"
  attribute_mapping = {
    "google.subject"           = "assertion.sub",
    "attribute.aud"            = "assertion.aud",
    "attribute.repository"     = "assertion.repository",
    "attribute.project_id"     = "assertion.project_id",
    "attribute.namespace_id"   = "assertion.namespace_id",
    "attribute.namespace_path" = "assertion.namespace_path",
    "attribute.ref"            = "assertion.ref",
    "attribute.ref_type"       = "assertion.ref_type",
  }
  oidc {
    issuer_uri = var.gitlab_url
    # allowed_audiences = [var.gitlab_url]  # This is used when you need to set the aud: value to match the JWT token.
  }
}

resource "google_service_account" "gitlab-runner" {
  account_id   = "gitlab-runner-service-account"
  display_name = "Service Account for GitLab Runner"
}

resource "google_service_account_iam_binding" "gitlab-runner-oidc" {
  service_account_id = google_service_account.gitlab-runner.name
  role               = "roles/iam.workloadIdentityUser"

  members = [
    "principalSet://iam.googleapis.com/${google_iam_workload_identity_pool.gitlab-pool.name}/attribute.project_id/${var.gitlab_project_id}"
  ]

}

output "GCP_WORKLOAD_IDENTITY_PROVIDER" {
  value = google_iam_workload_identity_pool_provider.gitlab-provider-jwt.name
}

output "GCP_SERVICE_ACCOUNT" {
  value = google_service_account.gitlab-runner.email
}